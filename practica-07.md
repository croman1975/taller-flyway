# Práctica 7

## Reparación

Al aplicar una migración, Flyway crea un "checksum" para comprobar la integridad del archivo SQL correspondiente a la migración y lo registra en la tabla **flyway_schema_history**.

Cada vez que se ejecuta el comando flyway migrate, se revisa el checksum de cada archivo contra dicha tabla y, en caso de encontrar diferencias, Flyway lanzará un mensaje de error y detendrá el proceso de migración.

## Comando "repair"

1. El comando ``flyway repair`` corrige los checksum en la tabla **flyway_schema_history** Sin embargo, no aplicará los cambios que se hayan agregado al archivo modificado.

2. Es muy importante tomar esto en cuenta y sobre todo no agregar modificaciones a migraciones que ya han sido aplicadas a la base de datos.

## Migraciones repetibles

Si necesitamos hacer cambios constantemente a un objeto de la base de datos, por ejemplo, una vista o un index, lo mejor es especificarlas en migraciones repetibles.

Una migración repetible es un script SQL tradicional, pero su nombre empieza con la letra R seguido de dos guiones bajos. Ejemplo:

``
R__Crear_vista_para_reporte.sql
``
Observa que no se les asigna un número de versión. Las migraciones repetibles aparecerán al final de la tabla de migraciones y se aplicarán en cada ejecución de ``flyway migrate``
