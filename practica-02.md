# Práctica 2

## Creación de la estructura de directorios para el taller.

1. Crea un directorio llamado "taller-flyway"

2. Dentro del directorio "taller-flyway" crea los siguientes subdirectorios: "software", "proyecto-1", "proyecto-2"

La estructura queda de la siguiente manera:
- taller-flyway
 - software
 - proyecto-1
 - proyecto-2

[ver estructura de directorios](https://gitlab.com/danielbg.dev/taller-flyway/-/blob/master/img/directorios.png)
