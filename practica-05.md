# Práctica 5

## Primera migración

1. En el directorio ``flyway-7.7.0/sql`` vamos a crear el primer script. Crea un archivo nuevo con el nombre **V001__Crea_tabla_01.sql**

```
- taller-flyway
  - proyecto-01
    - flyway-7.7.0
      - sql
        - V001__Crea_tabla_01.sql
```
### El nombrado de las migraciones
- El nombre del archivo empieza con **"V"** para indicarle a Flyway que se trata de un archivo de migración, la "V" es alusiva a "versión"

- El **001** indica el número de versión, así como el orden en el cual Flyway lo deberá aplicar a nuestro esquema de la base de datos.

- El doble guion bajo en el nombre del archivo **__** es muy importante para que Flyway identifique el archivo como una migración, un error común es omitir un guión bajo.

- A continuación, viene el nombre del archivo, este es arbitrario y puede ir separado por guiones medios o guiones bajos. Evita los espacios en blanco.

- Finalmente, el archivo debe tener la extensión **.sql**

Dentro del archivo crea una tabla llamada prueba con un campo llamado ID de tipo numérico entero y un campo nombre de tipo varchar de tamaño 30. Ejemplo en Postgres:

```
create table prueba (
    id int,
    nombre varchar(30)
);
```
2. En la línea de comandos, vuelve a ejecutar ``flyway info`` y en la tabla de migraciones deberías ver la nueva migración creada y que Flyway ha detectado. Deberá aparecer con el estatus **pending**.

3. En este punto no se ha aplicado la migración, solamente la ha detectado la herramienta. Para aplicarla, ejecuta el comando ``flyway migrate``

4. **Comprueba** que las migraciones han sido aplicadas con el comando ``flyway info`` Deberán aparecer con el estatus **success**.

5. **Ejercicio:** Crea 2 tablas nuevas en un nueva migración (un nuevo archivo SQL) y aplicalas a la base de datos con ``flyway migrate``
